#include <stdint.h>

#pragma once

struct pixel
{
    uint8_t r, g, b;
};

struct image
{
    uint64_t width, height;
    struct pixel *data;
};

struct image create_image(uint64_t width, uint64_t height);

struct image rotate(struct image image);
