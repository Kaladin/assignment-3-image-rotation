#include <bmp_format.h>
#include <bmp_use_cases.h>

#define BM 0x4d42
#define RESERVED 0
#define OFFSET 54
#define HEADER_SIZE 40
#define PLANES 1
#define COMPRESSION 0
#define PPM 2834
#define BIT_COUNT 24
#define COLORS_USED 0
#define COLORS_IMPORTANT 0

static int8_t calculate_padding(uint64_t const width)
{
    if (width % 4 == 0)
    {
        return 0;
    }

    return (int8_t)(4 - 3 * width % 4);
}

enum read_status from_bmp(FILE *in, struct image *img)
{

    if (in == NULL || img == NULL) {
        return READ_INVALID_BITS;
    }

    struct bmp_header bmpHeader;
    size_t result = fread(&bmpHeader, sizeof(bmpHeader), 1, in);

    // validate header
    if (result != 1)
    {
        return READ_INVALID_HEADER;
    }

    if (bmpHeader.bfType != BM)
    {
        return READ_INVALID_SIGNATURE;
    }

    if (bmpHeader.biBitCount != BIT_COUNT)
    {
        return READ_INVALID_BITS;
    }

    *img = create_image(bmpHeader.biWidth, bmpHeader.biHeight);

    int8_t padding = calculate_padding(img->width);

    for (uint32_t y = 0; y < img->height; y++)
    {
        if (fread(img->data + img->width * y, 3, img->width, in) != img->width)
        {
            return READ_INVALID_PIXELS;
        }

        // in case of padding
        if (fseek(in, padding, SEEK_CUR))
        {
            return READ_INVALID_PIXELS;
        }
    }

    return READ_OK;
}

enum write_status to_bmp(FILE *const out, struct image const *img)
{

    if (out == NULL || img == NULL) {
        return WRITE_ERROR;
    }

    struct bmp_header header;

    int8_t padding = calculate_padding(img->width);

    header.bfType = BM;
    header.biSizeImage = (img->width * 3 + padding) * img->height;
    header.bOffBits = OFFSET;
    header.bfileSize = header.biSizeImage + sizeof(struct bmp_header);
    header.biHeight = img->height;
    header.biWidth = img->width;
    header.biSize = HEADER_SIZE;
    header.biPlanes = PLANES;
    header.biCompression = COMPRESSION;
    header.bfReserved = RESERVED;
    header.biClrUsed = COLORS_USED;
    header.biClrImportant = COLORS_IMPORTANT;
    header.biBitCount = BIT_COUNT;
    header.biXPelsPerMeter = PPM;
    header.biYPelsPerMeter = PPM;

    if (!fwrite(&header, sizeof(struct bmp_header), 1, out))
    {
        return WRITE_ERROR;
    }

    for (uint64_t i = 0; i < img->height; i++)
    {
        fwrite(img->data + i * img->width, 3, img->width, out);

        fseek(out, padding, SEEK_CUR);
    }

    return WRITE_OK;
}
