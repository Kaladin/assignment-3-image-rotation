#include "bmp_use_cases.h"
#include <malloc.h>
#include <stdio.h>

int main(int argc, char **argv)
{
    if (argc != 3)
    {
        fprintf(stderr, "Incorrect number of arguments");
        return 1;
    }

    FILE *in = fopen(argv[1], "rb");
    if (in == NULL)
    {
        fprintf(stderr, "Can't open file %s for read", argv[1]);
        return READ_INVALID_BITS;
    }

    FILE *out = fopen(argv[2], "wb");
    if (out == NULL)
    {
        fprintf(stderr, "Couldn't open file %s for write", argv[2]);
        return READ_INVALID_BITS;
    }

    struct image image;
    enum read_status read_status = from_bmp(in, &image);

    if (read_status != READ_OK)
    {
        fprintf(stderr, "Error when reading source image\n");
        return READ_INVALID_BITS;
    }

    struct image rotated = rotate(image);
    enum write_status write_status = to_bmp(out, &rotated);

    if (write_status != WRITE_OK)
    {
        fprintf(stderr, "Error when writing image to disk\n");
        return READ_INVALID_SIGNATURE;
    }

    fclose(in);
    fclose(out);

    free(image.data);
    free(rotated.data);
    return 0;
}
