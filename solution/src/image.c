//
// Created by kaladin on 18.11.22.
//
#include "image.h"
#include <malloc.h>

struct image create_image(uint64_t width, uint64_t height)
{
    return (struct image){
        .width = width,
        .height = height,
        .data = malloc(sizeof(struct pixel) * height * width)};
}

static size_t get_index(uint64_t width, size_t i, size_t j)
{
    return (size_t)(i * width + j);
}

struct image rotate(struct image const image)
{
    struct image final = create_image(image.height, image.width);

    for (size_t i = 0; i < final.height; i++)
    {
        for (size_t j = 0; j < final.width; j++)
        {
            struct pixel sourcePixel = image.data[get_index(image.width, image.height - j - 1, i)];

            final.data[get_index(final.width, i, j)] = sourcePixel;
        }
    }

    return final;
}
